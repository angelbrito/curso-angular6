//import {v4 as uuid} from 'uuid';

export class DestinoViaje {
    /* Con public podemos quitar estos
    nombre:string;
    imagenUrl:string;
    */
    private selected: boolean;
    public servicios: string[];
	id: any;
    constructor(public nombre:string, public imagenUrl:string, public votes: number = 0) {
        this.selected = false;
        this.servicios = ['pileta', 'desayuno'];
    }
    isSelected(): boolean {
        return this.selected;
    }
    setSelected(s: boolean) {
        this.selected = s;
    }
    //Agregar directivas para mostrar mejor la información

    //Sistema de votos
    voteUp() {
		this.votes++;
	}

    voteDown() {
		this.votes--;
	}

}
//linter sugerencias de buenas prácticas
