//Uso del framework RXJS API de Subject y BehaviourSubject
import { BehaviorSubject, Subject } from 'rxjs'; //No se requiere descargar con npm porque viene con Angular
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import { DestinoViaje } from './destino-viaje.model';
import { Store } from '@ngrx/store';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.model';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable() //Haciendo injectable
export class DestinosApiClient {
	destinos: DestinoViaje[] = [];

	constructor(
		private store: Store<AppState>,
		@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
		private http: HttpClient
		) {
		this.store
		.select(state => state.destinos)
		.subscribe(data => {
		  console.log('destinos sub store');
		  console.log(data);
		  this.destinos = data.items;
		});
		this.store
		.subscribe(data => {
		  console.log('all store');
		  console.log(data);
		});
	  }
	
	add(d: DestinoViaje) { //Llamada al Web Services
    	const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'}); //Se envía información en formato JSON con el token
		const req = new HttpRequest('POST', this.config.apiEndPoint + '/my', { nuevo: d.nombre }, { headers: headers });
		this.http.request(req).subscribe((data) => {
			if ((data as HttpResponse<{}>).status === 200) { //Todo fue correcto
			  this.store.dispatch(new NuevoDestinoAction(d)); //Despachamos la nueva acción
			  const myDb = db; //Inicialización exportada
			  myDb.destinos.add(d);
			  console.log('todos los destinos de la DB!');
			  myDb.destinos.toArray().then((destinos: any) => console.log(destinos)); //API de Indexed DB: then cuando este listo
			}
		});
	}

	elegir(d: DestinoViaje) {
    	this.store.dispatch(new ElegidoFavoritoAction(d));
  	}

	getById(id: String): DestinoViaje {
		return this.destinos.filter(d => d.id.toString() === id)[0];
	}
	
	getAll(): DestinoViaje[] {
		return this.destinos;
	}
	
}
