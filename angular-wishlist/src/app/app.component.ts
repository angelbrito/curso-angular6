import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs'; //Se puede ocupar para hacer una consulta a un servidor, posición de GPS

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

//Rendereo Observable de HTML
//Rendereo diferido de HTML a través de intervalos

export class AppComponent {
  title = 'angular-wishlist';
  time = new Observable(observer => { //lo que le importa es cuando se llama el evento NEXT (observador de STRINGs)
    setInterval(() => observer.next(new Date().toString()), 1000); //Callback, cada 1 segundo llamar
  }); //Observable sobre un objeto con fat arrow function lambda

  constructor(public translate: TranslateService) {
    console.log('get translation');
    translate.getTranslation('en').subscribe(x => console.log('x: ' + JSON.stringify(x)));
    translate.setDefaultLang('es');
  }

  detinoAgregado(d: any){
    //alert(d.nombre)
  }
}