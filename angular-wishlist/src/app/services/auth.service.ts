//Ocupando el OUTSERVICE
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(user: string, password: string): boolean {
    if (user == 'user' && password == 'password') {
      localStorage.setItem('username', user); //Guardar valores en el navegador
      return true;
    }
    return false;
  }

  logout(): any {
    localStorage.removeItem('username');
  }

  getUser(): any {
    return localStorage.getItem('username');
  }

  isLoggedIn(): boolean { //Checa si es distinto de NULL
    return this.getUser() !== null;
  }
}
