import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient] //Inyección de dependencia directamente
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  updates : string[]; //Saber cual es el elegido
  
  all: any;

  //Agregando Redux
  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) { //Requiere ser public para que se acceda en el component
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
              .subscribe(d => {
                if (d != null) { //Porque se inicializa en null
                  this.updates.push('Se ha elegido a ' + d.nombre);
                }
              });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
  }
  
  //Para el nuevo evento
  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d)        ;
    this.onItemAdded.emit(d);
  }

  // Se dispara el evento
  elegido(e: DestinoViaje) { 
    this.destinosApiClient.elegir(e);
  }

  getAll() {

  }
}
