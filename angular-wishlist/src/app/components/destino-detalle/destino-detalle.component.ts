import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';

/* FUE para pruebas
// Se van a crear artefactos aquí, aunque se puede hacer directamente en el app.module.ts
class DestinosApiClientViejo {
  getById(id: String): DestinoViaje {
    console.log('llamando por la clase vieja!');
    return new DestinoViaje('', '');
  }
}

interface AppConfig {
  apiEndpoint: String;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'mi_api.com' //Valor puntual
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

//Decoradores
@Injectable() //Requiere esta llamada para que funcione
class DestinosApiClientDecorated extends DestinosApiClient {
  //config: variable de configuración
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
    super(store);
  }

  getById(id: String): DestinoViaje {
    console.log('llamando por la clase decorada!');
    console.log('config: ' + this.config.apiEndpoint);
    return super.getById(id);
  }

}
*/

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [ DestinosApiClient ] //Inyección de dependencia directamente
})
export class DestinoDetalleComponent implements OnInit {
  destino!: DestinoViaje;

  constructor(private route: ActivatedRoute, private destinosApiClient:DestinosApiClient) { }

  ngOnInit() {
	  let id = this.route.snapshot.paramMap.get('id');
    //this.destino = this.destinosApiClient.getById(id);
  }

}
