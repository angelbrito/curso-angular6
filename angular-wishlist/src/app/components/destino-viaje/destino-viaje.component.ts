import { Component, OnInit, Input, HostBinding, EventEmitter, Output  } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { VoteDownAction, VoteUpAction } from './../../models/destinos-viajes-state.model';

//aceptar parámetros externos
@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})

// wrap -> encapsular los modelos
export class DestinoViajeComponent implements OnInit {
  // Variable de nombre o título
  @Input() destino!: DestinoViaje;
  //Nombre elegido (renombrar)
  @Input('idx') position: number;
  
  //Vincular directamente un atributo HTML en el componente
  @HostBinding('attr.class') cssClass = 'col-md-4';
  
  // Propiedad (genérico con tipo)
  @Output() clicked: EventEmitter<DestinoViaje>;
  
  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
    this.position = 0;
  }

  ngOnInit(): void {
  }

  //Desencadenar eventos de hijos que los cachen los padres
  // Emitir eventos
  ir() {
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino)); //Despacho automático
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
