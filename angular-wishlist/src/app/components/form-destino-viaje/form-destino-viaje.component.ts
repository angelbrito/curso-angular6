import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators'; //Importar los operadores a ocupar
import { ajax } from 'rxjs/ajax';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>; //Evento de salida
  fg: FormGroup; //Grupo de elementos construidos
  minLongitud = 3; //Variable constante
  searchResults: string[]; //También se puede con ! para evitar inicializar en el constructor

  //Inicializar las variables         forwardRef para referencia circular
  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { //Inyección de dependencias (contruye el formulario)
    this.onItemAdded = new EventEmitter();
    this.searchResults = ['']; //Inicializar la variable
    // Validadores por defecto
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator
        //this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url: ['', Validators.required]
    }); //Que controles se van a vincular
    //Registrar un observable
    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio el formulario: ', form);
    });
  }

  ngOnInit(): void {
    //Levantar el texto para crear sugerencias
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input') //Subscribirnos cuando se aprieta una letra (escucha cada que el usuario toca una tecla)
      .pipe( //Flujo para operaciones en serie
        map((eventNames) => (eventNames.target as HTMLInputElement).value), //toda la cadena
        filter(text => text.length > 2),
        debounceTime(200), //Detener 200 milisegundos
        distinctUntilChanged(), //Hasta que sea algo distinto avanzar la sugerencia
        switchMap((text: string) => ajax(this.config.apiEndPoint + '/ciudades?q=' + text))
      ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response)
  }
  
  //El comportamiento que había en el componente de lista-destinos
  guardar(nombre: string, url:string):boolean {
    let d = new DestinoViaje(nombre, url); //const porque no cambia de valor
    this.onItemAdded.emit(d); //agregar el evento
    return false;
  }

  // Retorna un objeto 'required': true
  nombreValidator(control: FormControl): Object | null{ //recive un control  en el que se implementa la validcion y devolvemos un objeto key y un valor boolean
    //{[s:string]:boolean} se sustituye por Object | null
    const l = control.value.toString().trim().length;
    if(l > 0 && l < 5){
      return { 'invalidNombre': true }
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    (control: FormControl): Object | null => {
      let l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { 'minLongNombre': true };
      }
      return null;
    }
    return Object;
  }
}
