import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router' // Hacer routing
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; //Agregando los nuevos objetos
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http'

import Dexie from 'dexie';

import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';

import { 
  DestinosViajesEffects,
  DestinosViajesState,
  InitMyDataAction,
  intializeDestinosViajesState,
  reducerDestinosViajes
} from './models/destinos-viajes-state.model';
import { ActionReducerMap, StoreModule as NgRxStoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { DestinoViaje } from './models/destino-viaje.model';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';

//Inyección de dependencias de variables de configuración
export interface AppConfig {
  apiEndPoint: String;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'http://localhost:3000' //Variable de archivo de configuración, SO, servidor, API
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export const childrenRoutesVuelos: Routes = [ //Rutas hijas de vuelo se cargan como rutas raíz
  { path: '', redirectTo: 'main', pathMatch: 'full'},
  { path: 'main', component: VuelosMainComponentComponent},
  { path: 'mas-info', component: VuelosMasInfoComponentComponent},
  { path: ':id', component: VuelosDetalleComponentComponent },
];

// Definir las rutas de la aplicación (en la barra de dirección)
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: ListaDestinosComponent},
  { path: 'destino/:id', component: DestinoDetalleComponent}, //los dos puntos son su tokens para la parametrización por URL
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard ]
  },
  {
    path: 'vuelos',
    component: VuelosComponentComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    children: childrenRoutesVuelos //Tiene hijos
  }
];

//INIT de REDUX
export interface AppState {
  destinos: DestinosViajesState;
}

const reducers: ActionReducerMap<AppState> = { //Unión de todos los estados
  destinos: reducerDestinosViajes
};

let reducerInitialState = {
  destinos: intializeDestinosViajesState()
};

// app init Cuando cargue la página aparezca por defecto todos los destinos guardados en la la API (servidor)
export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.intializeDestinosViajesState(); //Se invoca al objeto con el método
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async intializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint + '/my', { headers: headers }); //Ahora se llama al Web Services
    const response: any = await this.http.request(req).toPromise(); //Otra técnica sin subscribe
    this.store.dispatch(new InitMyDataAction(response.body)); //body es un array de strings
  }
}

// Dexie DB
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  destinos!: Dexie.Table<DestinoViaje, number>; //! para inicializar la variable con Indexeed DB
  translations!: Dexie.Table<Translation, number>;
  constructor() {
    super('MyDatabase');
    //Versionado de base de datos
    this.version(1).stores({
      destinos: 'id, nombre, imagenUrl', //variables tendrán las de inicialización
    });
    this.version(2).stores({
      destinos: 'id, nombre, imagenUrl', //variables tendrán las de inicialización
      translations: 'id, lang, key, value'
    });
  }
}
export const db = new MyDatabase();

//I18N init
class TranslationLoader implements TranslationLoader { //Cargador personalizado de traducciones
  constructor(private http: HttpClient) {}

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
                      .where('lang') //Consultas
                      .equals(lang) //igualación
                      .toArray()
                      .then(results => { //away o callback
                        if (results.length === 0) {
                          return this.http //llamada AJAX
                                  .get<Translation[]>(APP_CONFIG_VALUE.apiEndPoint + '/api/translation?lang=' + lang)
                                  .toPromise()
                                  .then(apiResults => {
                                    db.translations.bulkAdd(apiResults); //Agregar a las traducciones
                                    return apiResults;
                                  });
                        }
                        return results;
                      }).then(traducciones => {
                        //Solo loguear y devolver las traducciones
                        console.log('traducciones cargadas');
                        console.log(traducciones);
                        return traducciones;
                      }).then(traducciones => {
                        return traducciones.map(t => ({[t.key] : t.value})); //Del modo que requiere JSON
                      });
    return from(promise).pipe(flatMap((elems) => from(elems))); //como se tiene un array de arrays se hace un flatMap
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}

@NgModule({ // Decorador agrega los metadatos a la siguiente clase (este es un paquete)
  declarations: [ //Propiedades
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent
  ],
  imports: [ //Importación
    BrowserModule, //aplicación navegador
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes), //registrar las rutas
    NgRxStoreModule.forRoot(reducers, { /* initialState: reducerInitialState //NO FUNCIONA */ }),
    EffectsModule.forRoot([DestinosViajesEffects]), //Todos los effects deseados
    //Agregando Redux DevTools
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    TranslateModule.forRoot({
      loader: { //Cargador de traducciones
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [ //Inyección de dependencia 
    AuthService, UsuarioLogueadoGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
    //Injection Token de Angular, función init_app: va a retornar un objeto con ciertas tareas, se tiebe dependencia con AppLoadService
    // Se pueden tener varios códigos de inicilización
    MyDatabase
  ], 
  bootstrap: [AppComponent] //Iniciar la aplicación
})
export class AppModule { }
