import { Injectable } from '@angular/core';

@Injectable({ //Es el que usa la directiva Inject implícita en el constructor
  providedIn: 'root'
})
export class ReservasApiClientService {
  constructor() { } 

  getAll() {
    return [ { id: 1, name: 'uno'}, {id: 2, name: 'dos'} ];
  }
}
