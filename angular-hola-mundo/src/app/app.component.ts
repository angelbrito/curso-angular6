// Viene a este archivo para definir el componente
import { Component } from '@angular/core';

@Component({
  selector: 'app-root', //Como va ser el tag
  templateUrl: './app.component.html', //Página
  styleUrls: ['./app.component.css'] //Estilos
})
// Encapsulado de comportamiento
export class AppComponent { // Definir el comportamiento
  title = 'angular-hola-mundo';
}
