var express = require("express"), cors = require('cors'); //Requiriendo el módulo
var app = express(); //Inicializar el servidor
app.use(express.json()); //Ocupar JSON
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000")); //Ocuapr el puerto 3000

// Se tienen tres rutas
var ciudades = [ "Paris", "Barcelona", "Barranquilla", "Montevideo", "Santiago de Chile", "Ciudad de México", "Nueva York" ] //Se levanat de un servidor
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

var misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
    console.log(req.body);
    misDestinos.push(req.body.nuevo);
    res.json(misDestinos);
});

//Nuevo web services
app.get("/api/translation", (req, res, next) => res.json([ 
    {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang}
]));
